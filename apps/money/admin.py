
from django.contrib import admin

from apps.money.models import *


class PerfectAddAdmin(admin.ModelAdmin):
    list_display = ('id','user','money','key','visible',)

class PerfectSubtractAdmin(admin.ModelAdmin):
    list_display = ('id','user','money','purse','visible',)

class PayeerSubtractAdmin(admin.ModelAdmin):
    list_display = ('id','user','money','purse','visible',)

class PayeerAddAdmin(admin.ModelAdmin):
    list_display = ('id','user','money','key','visible',)

class TransferObtainingMoneyAdmin(admin.ModelAdmin):
    list_display = ('id','user','user_name','money','visible',)

class InternalTranslationAdmin(admin.ModelAdmin):
    list_display = ('id','user','moneys','visible',)

# class TransferPiggyBanksAdmin(admin.ModelAdmin):
#     list_display = ('id','user','moneys',)
#
# class TransferExpenMoneyAdmin(admin.ModelAdmin):
#     list_display = ('id','user','money','payments','datetime',)
#

admin.site.register(PerfectAdd, PerfectAddAdmin)
admin.site.register(PerfectSubtract, PerfectSubtractAdmin)
admin.site.register(PayeerAdd, PayeerAddAdmin)
admin.site.register(PayeerSubtract, PayeerSubtractAdmin)

admin.site.register(TransferObtainingMoney, TransferObtainingMoneyAdmin)
admin.site.register(InternalTranslation, InternalTranslationAdmin)
# admin.site.register(TransferPiggyBanks, TransferPiggyBanksAdmin)
# admin.site.register(TransferExpenMoney, TransferExpenMoneyAdmin)
