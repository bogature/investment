# coding=utf-8
import datetime
from _sha256 import sha256

from django.contrib.auth.decorators import login_required
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.db.models import F
from django.shortcuts import render, redirect

from apps.money.PerfectMoney import PerfectMoney
from apps.payeer.api import payeer_api

from apps.money.forms import *
from apps.user.models import Profile

from apps.payeer.constants import CURRENCY_USD


def get_balance(account, password):
    pm = PerfectMoney(account, password)
    res = pm.balance()
    return res

def transfer_moneys(account, password, payer, payee, amount, memo, payment_id):
    pm = PerfectMoney(account, password)
    res = pm.transfer(payer, payee, amount, memo, payment_id)
    return res


@login_required
def refill(request):

    print("Chec")

    # from coinpayments import CoinPaymentsAPI
    # api = CoinPaymentsAPI(public_key='f6cc952c2f0c60e7287141cfc649ed913d7bb2f70ecf461d016f2de6f5509b58',
    #                       private_key='31aAA72748Aea82c08993997d0Eb57A20734cAe44872803aA52752E3D4885654')

    # from coinpayments import CoinPaymentsAPI
    # api = CoinPaymentsAPI(public_key='e6a939f5210909d8b592f70b146a736d23dccd19364877be808860a773a441d6',
    #                       private_key='E1291680A662FcC5D1E506B228b0BdEC744Ccf932c5292ca9976451D555e997A')
    #
    # print(api.check_signature('CPDE1AC7E7BJSEZM8S0WOA1WTK', 'CPDE1AC7E7BJSEZM8S0WOA1WTK'))
    # print(api.get_deposit_address())
    # print(api.rates())
    # print(api.get_basic_info())

    # print(api.check_signature('CPDE4KQKM2UNTPLOBESWTOCQBV','CPDE4KQKM2UNTPLOBESWTOCQBV'))
    # print(api.rates())
    # print(api.check_signature('3MUb4BQ5ZUsS3QGDa9ojrG6AeaSVHxqN6g', 'CPDE4KQKM2UNTPLOBESWTOCQBV'))
    # api.rates()
    # api.check_signature('ipn_message', 'ipn signautre')
    # api.check_signature('ipn_message', 'ipn signautre')

    # print("End chec")

    context = {
    }
    return render(request, 'refill.html', context)


@login_required
def issuance(request):

    context = {
    }
    return render(request, 'issuance.html', context)



@login_required
def perfect_admin(request):
    account = '3310706'
    password = 'zN3Ht7vV'

    pm = PerfectMoney(account, password)
    res = pm.balance()
    pr_balans_perfect = str(res.get('U19769559'))

    all_perfects = PerfectSubtract.objects.filter(visible=True)

    if request.method == "POST":
        pos_id = request.POST.get('position')

        perfect_sub = PerfectSubtract.objects.filter(pk=(int(pos_id)))[0]

        if (perfect_sub.visible == True):
            PerfectSubtract.objects.filter(pk=perfect_sub.pk).update(end=True, visible=False)

            account = '3310706'
            password = 'zN3Ht7vV'

            payer = 'U19769559'

            count = PerfectSubtract.objects.all().count()
            name_user = perfect_sub.user

            money = perfect_sub.money
            user_payer = perfect_sub.purse

            komisia = (float(perfect_sub.money) * float(3.5) / 100)
            print(komisia)
            endmoney = (money - komisia)
            print(endmoney)

            get_balance(account, password)
            if perfect_sub.refka == False:

                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(zarab_usd=F("zarab_usd") - perfect_sub.money)
                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(out_usd=F("out_usd") + perfect_sub.money)

            else:

                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    bonus_usd=F("bonus_usd") - perfect_sub.money)
                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    out_bonus_usd=F("out_bonus_usd") + perfect_sub.money)

            transfer_moneys(account, password, payer, user_payer, round(endmoney, 2), str(name_user), int(count))

            return redirect(request.path)

        else:
            return redirect('/money/transfer/no/')

    context = {

        'pr_balans_perfect':pr_balans_perfect,
        'all_perfects':all_perfects,

    }

    return render(request, 'perfect_admin.html', context)



@login_required
def refill_perfect(request):

    money = '0'
    url = ''
    sing = ''
    name_button = 'Подтвердить'

    end_refill_perfect = PerfectAdd.objects.filter(user=request.user, visible=True).order_by('-id')

    perfect_count = PerfectAdd.objects.all().count()

    form = PerfectAddForm
    if request.method == "POST":
        form = PerfectAddForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            now = datetime.datetime.now()
            inst.datetime = now

            PAYEE_ACCOUNT = 'U19769559'
            PAYEE_NAME = 'Deltus.biz'
            PAYMENT_UNITS = 'USD'
            STATUS_URL = 'http://deltus.biz/money/refill-perfect/'
            PAYMENT_URL = 'http://deltus.biz/money/refill-perfect/ok/'
            NOPAYMENT_URL = 'http://deltus.biz/ru/info/'
            PAYMENT_AMOUNT = str(inst.money) + str(request.user) + str(perfect_count)

            hash = "{}:{}:{}:{}:{}:{}:{}".format(PAYEE_ACCOUNT,
                                                 PAYEE_NAME,
                                                 PAYMENT_UNITS,
                                                 STATUS_URL,
                                                 PAYMENT_URL,
                                                 NOPAYMENT_URL,
                                                 PAYMENT_AMOUNT
                                                 )

            sign_hash = sha256(hash.encode())
            sing = sign_hash.hexdigest().upper()


            inst.key = sing
            inst.save()

            url = 'https://perfectmoney.is/api/step1.asp'
            money = inst.money
            name_button = 'Оплатить'


    context = {

        'sing': sing,
        'url': url,
        'form': form,
        'money': money,
        'name_button': name_button,
        'end_refill_perfect': end_refill_perfect,

    }

    return render(request, 'refill_perfect.html', context)


@login_required
def merchant(request):

    return render(request, 'refill_perfect.html')


def refill_perfect_ok(request):

    context = {
    }

    return render(request, 'refill_perfect_ok.html', context)


@login_required
def refill_perfect_add(request, perfect_pk):

    refill = PerfectAdd.objects.filter(user=request.user, visible=False).order_by('-id')[0]
    if (refill.visible == False and refill.key == perfect_pk):
        PerfectAdd.objects.filter(pk=refill.pk).update(visible=True)

        Profile.objects.filter(user=refill.user).update(balance_usd=F('balance_usd') + float(refill.money))
        Profile.objects.filter(user=refill.user).update(filled_usd=F('filled_usd') + float(refill.money))

    context = {

        'refill': refill,

    }
    return render(request, 'refill_perfect_add.html', context)


def toFixed(numObj, digits=0):
    return f"{numObj:.{digits}f}"


@login_required
def refill_payeer(request):

    end_refill_payeers = PayeerAdd.objects.filter(user=request.user, end=True).order_by('-id')

    form = PayeerAddForm
    if request.method == "POST":
        form = PayeerAddForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user

            count_payeer = PayeerAdd.objects.all().count()

            description = 'Popolnenia ' + str(inst.user)

            data = payeer_api.merchant(
                order_id=count_payeer,
                amount=inst.money,
                currency=CURRENCY_USD,
                description=description
            )

            inst.order_id = count_payeer
            inst.amount = inst.money
            inst.currency = CURRENCY_USD
            inst.description = description

            import datetime
            dat = datetime.datetime.now()

            hash = "{}:{}:{}:{}:{}:{}:{}:{}:{}".format(
                count_payeer,
                CURRENCY_USD,
                description,
                'DRiej12pXOM3KoHc',
                'DRiejdd12pXOM3KoHcUsas',
                float(1111),
                dat.day,
                dat.month,
                dat.year,
                request.user
            )

            sign_hash = sha256(hash.encode())
            sing = sign_hash.hexdigest().upper()

            inst.key = str(sing)

            now = datetime.datetime.now()
            inst.datetime = now

            inst.save()

            return redirect(data.get('location'))

    context = {

        'end_refill_payeers': end_refill_payeers,
        'form': form,

    }

    return render(request, 'refill_payeer.html', context)


@login_required
def refill_payeer_add(request):

    m_orderid = str(request.GET.get('m_orderid'))
    description = 'Popolnenia ' + str(request.user)
    dat = datetime.datetime.now()

    hash = "{}:{}:{}:{}:{}:{}:{}:{}:{}".format(
        m_orderid,
        CURRENCY_USD,
        description,
        'DRiej12pXOM3KoHc',
        'DRiejdd12pXOM3KoHcUsas',
        float(1111),
        dat.day,
        dat.month,
        dat.year,
        request.user
    )

    sign_hash = sha256(hash.encode())
    sing = sign_hash.hexdigest().upper()

    # description = 'Popolnenia ' + str(request.user)
    # order_id = request.GET.get('m_orderid')
    # amount = request.GET.get('m_amount')
    # currency = request.GET.get('m_curr')
    # desc_str = payeer_api.generate_description(description)
    #
    # all_end_sign = payeer_api.generate_signature(order_id, amount, currency, desc_str)
    #
    #
    #
    # description = 'Popolnenia ' + str(request.user)
    # m_orderid = request.GET.get('m_orderid')
    # m_amount = request.GET.get('m_amount')
    # m_curr = request.GET.get('m_curr')
    # m_desc = request.GET.get('m_desc')
    # # Генерация сигнатури
    # descrip = payeer_api.generate_description(description)
    #
    # my_refill = PayeerAdd.objects.filter(user=request.user).order_by('-id')[0]
    #
    # a_sign = payeer_api.generate_signature(m_orderid, m_amount, m_curr, m_desc)
    # b_sign = payeer_api.generate_signature(my_refill.order_id, toFixed(my_refill.amount, 2), my_refill.currency, m_desc)
    #



    # new_sign = '111'
    #
    # m_operation_id = str(request.GET.get('m_sign'))
    # status = ''
    #
    # m_orderid = str(request.GET.get('m_orderid'))
    # m_amount = str(request.GET.get('m_amount'))
    #
    # signature = request.GET.get('m_sign')
    #
    # description = 'Popolnenia ' + str(request.user)
    #
    # import datetime
    # dat = datetime.datetime.now()
    #
    # hash = "{}:{}:{}:{}:{}:{}:{}:{}".format(
    #     m_orderid,
    #     CURRENCY_USD,
    #     description,
    #     'DRiej12pXOM3KoHc',
    #     'DRiejdd12pXOM3KoHcUsas',
    #     float(1111),
    #     dat.day,
    #     dat.month,
    #     dat.year
    # )
    #
    # sign_hash = sha256(hash.encode())
    # sing = sign_hash.hexdigest().upper()
    #
    refill = PayeerAdd.objects.filter(user=request.user, visible=False, end=False).order_by('-id')[0]

    status = "***"

    if (refill.visible == False and refill.key == sing and refill.user == request.user):
        PayeerAdd.objects.filter(pk=refill.pk).update(visible=True, end=True)

        Profile.objects.filter(user=refill.user).update(balance_usd=F('balance_usd') + float(refill.money))
        Profile.objects.filter(user=refill.user).update(filled_usd=F('filled_usd') + float(refill.money))

        # Profile.objects.filter(user=refill.user).update(expense=F('expense') + float(refill.money))
        # Profile.objects.filter(user=refill.user).update(pay_add=F('pay_add') + float(refill.money))
        status = "Пополнен успешно !"


    context = {

        'status':status,
    }

    return render(request, 'refill_payeer_add.html', context)


# Вивод Perfect Money
@login_required
def issuance_perfect(request):
    massage = ''
    data = datetime.datetime.today().weekday()

    contribution_usd = request.user.profile.deposit_usd * 4

    perfect_subtracts = PerfectSubtract.objects.filter(user=request.user, end=False).order_by('-id')
    end_perfect_subtracts = PerfectSubtract.objects.filter(user=request.user, end=True).order_by('-id')

    form = PerfectSubtractForm
    if request.method == "POST":
        variant_vivoda = request.POST.get('variant_vivoda')
        print(variant_vivoda)

        form = PerfectSubtractForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            inst.purse = request.user.profile.perfectmoney
            inst.visible = True
            now = datetime.datetime.now()
            inst.datetime = now

            if (request.user.profile.perfectmoney != None):

                if (int(variant_vivoda) == 1):
                    inst.refka = False
                    if (int(data) == 4):
                        if (inst.money > 0):
                            if (request.user.profile.zarab_usd >= inst.money):
                                inst.save()
                                return redirect('/money/transfer/ok/vivod/')
                            else:
                                return redirect('/money/transfer/no/')
                        else:
                            return redirect('/money/transfer/no/')

                    else:
                        massage = "Баланс USD можно выводить в пятницу"

                if (int(variant_vivoda) == 2):
                    inst.refka = True
                    if ((request.user.profile.out_bonus_usd + inst.money) <= request.user.profile.deposit_usd * 4):

                        if (inst.money > 0):
                            if (request.user.profile.bonus_usd >= inst.money):
                                inst.save()
                                return redirect('/money/transfer/ok/vivod/')
                            else:
                                return redirect('/money/transfer/no/')
                        else:
                            return redirect('/money/transfer/no/')

                    else:
                        rez_sum = (request.user.profile.deposit_usd * 4) - request.user.profile.out_bonus_usd
                        massage = "С баланса (Партнерские вознаграждения) максимум можно вивисти $" + str(rez_sum) + " Сейчас доступно: " + str(request.user.profile.bonus_usd)

            else:
                massage = "Необходимо заполнить поле Perfectmoney в настройках"

    context = {

        'form': form,
        'perfect_subtracts': perfect_subtracts,
        'end_perfect_subtracts': end_perfect_subtracts,
        'contribution_usd': contribution_usd,
        'data': data,
        'massage': massage,

    }
    return render(request, 'issuance_perfect.html', context)


# Вывод Payeer
def issuance_payeer(request):
    massage = ""
    data = datetime.datetime.today().weekday()
    print(data)

    payeer_subtracts = PayeerSubtract.objects.filter(user=request.user, end=False).order_by('-id')
    end_payeer_subtracts = PayeerSubtract.objects.filter(user=request.user, end=True).order_by('-id')

    form = PayeerSubtractForm
    if request.method == "POST":
        variant_vivoda = request.POST.get('variant_vivoda')
        print(variant_vivoda)

        form = PayeerSubtractForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            inst.visible = True
            inst.purse = request.user.profile.payeer
            now = datetime.datetime.now()
            inst.datetime = now

            if (request.user.profile.payeer != None):

                if (int(variant_vivoda) == 1):
                    inst.refka = False
                    if (int(data) == 4):
                        if (inst.money > 0):
                            if (request.user.profile.zarab_usd >= inst.money):
                                inst.save()
                                return redirect('/money/transfer/ok/vivod/')
                            else:
                                return redirect('/money/transfer/no/')
                        else:
                            return redirect('/money/transfer/no/')

                    else:
                        massage = "Баланс USD можно выводить в пятницу"

                if (int(variant_vivoda) == 2):
                    inst.refka = True
                    # sum_vivoda = request.user.profile.oborot_usd * 4 - request.user.profile.out_bonus_usd

                    # print("Summa === ")
                    # print(sum_vivoda)

                    if ((request.user.profile.out_bonus_usd + inst.money) <= request.user.profile.deposit_usd * 4):
                        if (inst.money > 0):
                            if (request.user.profile.bonus_usd >= inst.money):
                                inst.save()
                                return redirect('/money/transfer/ok/vivod/')
                            else:
                                return redirect('/money/transfer/no/')
                        else:
                            return redirect('/money/transfer/no/')

                    else:
                        rez_sum = (request.user.profile.deposit_usd * 4) - request.user.profile.out_bonus_usd
                        massage = "С баланса (Партнерские вознаграждения) максимум можно вивисти $" + str(rez_sum) + " Сейчас доступно: " + str(request.user.profile.bonus_usd)
            else:
                massage = "Необходимо заполнить поле Payeer в настройках"
    context = {

        'form':form,
        'payeer_subtracts': payeer_subtracts,
        'end_payeer_subtracts': end_payeer_subtracts,
        'data': data,
        'massage': massage,

    }

    return render(request, 'issuance_payeer.html', context)

@login_required
def transfer_money(request):

    context = {

    }

    return render(request, 'transfer_money.html', context)


@login_required
def transferkopilka(request):

    transfer_piggy_bank = TransferPiggyBanksForm
    if request.method == "POST":
        form_transfer_pig = TransferPiggyBanksForm(request.POST)
        if form_transfer_pig.is_valid():
            insts2 = form_transfer_pig.save(commit=False)
            insts2.user = request.user

            if (insts2.moneys > 0):

                if (request.user.profile.bali > insts2.moneys):

                    insts2.save()
                    Profile.objects.filter(pk=request.user.profile.pk).update(bali=F('bali') - float(insts2.moneys))
                    Profile.objects.filter(pk=request.user.profile.pk).update(expense=F('expense') + float(insts2.moneys))

                    return redirect('/money/transfer/ok/')
                else:
                    return redirect('/money/transfer/no/')
            else:
                return redirect('/money/transfer/no/')

    context = {

        'transfer_piggy_bank': transfer_piggy_bank,

    }

    return render(request, 'transferkopilka.html', context)


@login_required
def transfervivod(request):

    form_transfer = InternalTranslationForm
    if request.method == "POST":
        form_transfer = InternalTranslationForm(request.POST)
        if form_transfer.is_valid():
            insts = form_transfer.save(commit=False)
            insts.user = request.user

            if (insts.moneys > 0):
                if (request.user.profile.obtaining_money > insts.moneys):

                    insts.save()
                    Profile.objects.filter(pk=request.user.profile.pk).update(obtaining_money=F('obtaining_money') - float(insts.moneys))
                    Profile.objects.filter(pk=request.user.profile.pk).update(expense=F('expense') + float(insts.moneys))


                    return redirect('/money/transfer/ok/')
                else:
                    return redirect('/money/transfer/no/')
            else:
                return redirect('/money/transfer/no/')

    context = {

        'form_transfer': form_transfer,

    }

    return render(request, 'transfervivod.html', context)


@login_required
def transferuser(request):

    form = TransferObtainingMoneyForm
    if request.method == "POST":
        form = TransferObtainingMoneyForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user

            if (inst.money > 0):

                if (request.user.profile.obtaining_money > inst.money):

                    users = User.objects.all()
                    for item in users:
                        if (item.username == inst.user_name):
                            Profile.objects.filter(pk=request.user.profile.pk).update(obtaining_money=F('obtaining_money') - inst.money)
                            Profile.objects.filter(pk=item.profile.pk).update(expense=F('expense') + float(inst.money))

                    inst.save()
                    return redirect('/money/transfer/ok/')
                else:
                    return redirect('/money/transfer/no/')
            else:
                return redirect('/money/transfer/no/')


    context = {

        'form': form,

    }

    return render(request, 'transferuser.html', context)


@login_required
def transfer_money_ok(request):

    context = {

    }

    return render(request, 'transfer_money_ok.html', context)


@login_required
def transfer_money_ok_vivod(request):

    context = {

    }

    return render(request, 'transfer_money_ok_vivod.html', context)


@login_required
def transfer_money_no(request):

    context = {

    }

    return render(request, 'transfer_money_no.html', context)


@login_required
def transfer_money_expense_in_bali(request):



    form_expense = TransferExpenseForm
    if request.method == "POST":
        form_expense = TransferExpenseForm(request.POST)
        if form_expense.is_valid():
            inst = form_expense.save(commit=False)
            inst.user = request.user
            inst.datetime = datetime.datetime.now()

            if (inst.money > 0.9 and request.user.profile.expense >= inst.money):

                inst.save()
                return redirect('/money/transfer/expense/bali/basket/')

            else:
                return redirect('/money/transfer/no/')

    all_transfer_expense = TransferExpenMoney.objects.filter(user=request.user, payments=True).order_by('-id')
    paginator = Paginator(all_transfer_expense, 10)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)


    context = {

        'form_expense':form_expense,
        'all_transfer_expenses': items,

    }

    return render(request, 'transfer_money_expense_in_bali.html', context)


@login_required
def transfer_money_expense_in_bali_basket(request):

    all_transfer_expenses = TransferExpenMoney.objects.filter(user=request.user, payments=False).order_by('-id')

    if request.method == "POST":
        pos_id = request.POST.get('position')

        pos = TransferExpenMoney.objects.get(pk=int(pos_id))

        if (pos.money > 0.9 and request.user.profile.expense >= pos.money):

            if (pos.payments == False):
                TransferExpenMoney.objects.filter(pk=int(pos_id)).update(payments=True)

                Profile.objects.filter(pk=pos.user.profile.pk).update(expense=F('expense') - float(pos.money))
                Profile.objects.filter(pk=pos.user.profile.pk).update(bali=F('bali') + float(pos.money))

                return redirect('/money/transfer/expense/bali/')

        else:
            return redirect('/money/transfer/no/')



    context = {

        'all_transfer_expenses':all_transfer_expenses,


    }

    return render(request, 'transfer_money_expense_in_bali_basket.html', context)




    # refill_form = RefillForm
    # if request.method == "POST":
    #     form = RefillForm(request.POST)
    #     if form.is_valid():
    #         inst = form.save(commit=False)
    #
    #         account = inst.account
    #         password = inst.password
    #         payer = inst.payer
    #         money = inst.money
    #
    #         pm = PerfectMoney(account, password)
    #
    #         if pm:
    #             print pm.error

            # if (not pm.error):
            #
                # transfer_money(account, password, payer, 'U15804314', money, 'popovnenia', 123)
                # Profile.objects.filter(pk=request.user.profile.pk).update(expense=F('expense') + inst.money)
            #
            #     inst.user = request.user
            #     inst.save()
            #
            # else:
            #     print 'sori'


            # return redirect('/')

    # account_number = 'P86938486'
    # api_id = '481680840'
    # apiKay = '2eZzmCA2jjTs8Gks'
    #
    # p = PayeerAPI(account_number, api_id, apiKay)
    # p.transfer(sum=1, to='P87263901', cur_in='RUB', cur_out='RUB',)





# Перевод средств с Баланса Вивода на Баланс активации
@login_required
def transfer_money_vivod_in_activation(request):

    transfer_money_vivod_in_activation_list = InternalTranslation.objects.filter(user=request.user, visible=False).order_by('-id')

    paginator = Paginator(transfer_money_vivod_in_activation_list, 10)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    form = TransferVivodInActivationForm
    if request.method == "POST":
        form = TransferVivodInActivationForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            inst.datetime = datetime.datetime.now()
            inst.visible = True

            Profile.objects.filter(user=request.user).update(transfer_obtaining_money=F('transfer_obtaining_money') + float(inst.moneys))

            if (inst.moneys > 0.9 and request.user.profile.obtaining_money >= inst.moneys):

                inst.save()
                return redirect('/money/transfer/obtaining/in/expense/basket/')

            else:
                return redirect('/money/transfer/no/')

    context = {

        'form':form,
        'transfer_money_vivod_in_activation_list':items,

    }

    return render(request, 'transfer_money_vivod_in_activation.html', context)


# Перевод средств с Баланса Вивода на Баланс активации корзина
@login_required
def transfer_money_vivod_in_activation_basket(request):

    all_transfer_vivod = InternalTranslation.objects.filter(user=request.user, visible=True).order_by('-id')

    if request.method == "POST":
        pos_id = request.POST.get('position')

        pos = InternalTranslation.objects.get(pk=int(pos_id))

        if (pos.moneys > 0.9 and request.user.profile.obtaining_money >= pos.moneys):

            if (pos.visible == True):
                InternalTranslation.objects.filter(pk=int(pos_id)).update(visible=False)

                Profile.objects.filter(pk=pos.user.profile.pk).update(obtaining_money=F('obtaining_money') - float(pos.moneys))
                Profile.objects.filter(pk=pos.user.profile.pk).update(expense=F('expense') + float(pos.moneys))

                return redirect('/money/transfer/obtaining/in/expense/')

        else:
            return redirect('/money/transfer/no/')


    context = {

        'all_transfer_vivod':all_transfer_vivod,

    }

    return render(request, 'transfer_money_vivod_in_activation_basket.html', context)




# Перевод средств с Баланса Вивода на Баланс активации
@login_required
def transfer_bali_in_activation(request):

    transfer_money_vivod_in_activation_list = TransferPiggyBanks.objects.filter(user=request.user, visible=False).order_by('-id')

    paginator = Paginator(transfer_money_vivod_in_activation_list, 10)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    form = TransferBaliInActivationForm
    if request.method == "POST":
        form = TransferBaliInActivationForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            inst.datetime = datetime.datetime.now()
            inst.visible = True

            if (inst.moneys > 0 and request.user.profile.bali > inst.moneys):

                inst.save()
                return redirect('/money/transfer/bali/in/expense/basket/')

            else:
                return redirect('/money/transfer/no/')

    context = {

        'form':form,
        'transfer_money_vivod_in_activation_list':items,

    }

    return render(request, 'transfer_bali_in_activation.html', context)


# Перевод средств с Баланса Вивода на Баланс активации корзина
@login_required
def transfer_bali_in_activation_basket(request):

    all_transfer_vivod = TransferPiggyBanks.objects.filter(user=request.user, visible=True).order_by('-id')

    if request.method == "POST":
        pos_id = request.POST.get('position')

        pos = TransferPiggyBanks.objects.get(pk=int(pos_id))

        if (pos.moneys > 0.9 and request.user.profile.bali >= pos.moneys):

            if (pos.visible == True):
                TransferPiggyBanks.objects.filter(pk=int(pos_id)).update(visible=False)

                Profile.objects.filter(pk=pos.user.profile.pk).update(bali=F('bali') - float(pos.moneys))
                Profile.objects.filter(pk=pos.user.profile.pk).update(expense=F('expense') + float(pos.moneys))

                return redirect('/money/transfer/bali/in/expense/')

        else:
            return redirect('/money/transfer/no/')


    context = {

        'all_transfer_vivod':all_transfer_vivod,

    }

    return render(request, 'transfer_bali_in_activation_basket.html', context)


# Отправка другому пользователю
@login_required
def transfer_vivod_in_activation_user(request):

    transfers = TransferObtainingMoney.objects.filter(user=request.user, visible=False).order_by('-id')

    paginator = Paginator(transfers, 10)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    form = TransferVivodInActivationUserForm
    if request.method == "POST":
        form = TransferVivodInActivationUserForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            inst.user = request.user
            inst.datetime = datetime.datetime.now()
            inst.visible = True

            if (inst.money > 0.9 and request.user.profile.expense >= inst.money):

                inst.save()
                return redirect('/money/transfer/obtaining/in/expense/user/basket/')

            else:
                return redirect('/money/transfer/no/')

    context = {

        'form': form,
        'transfers': transfers,

    }

    return render(request, 'transfer_vivod_in_activation_user.html', context)


# Отправка другому пользователю (корзина)
@login_required
def transfer_vivod_in_activation_user_basket(request):

    transfers = TransferObtainingMoney.objects.filter(user=request.user, visible=True).order_by('-id')

    if request.method == "POST":
        pos_id = request.POST.get('position')

        pos = TransferObtainingMoney.objects.get(pk=int(pos_id))

        if (pos.money > 0.9 and request.user.profile.expense >= pos.money):

            if (pos.visible == True):
                TransferObtainingMoney.objects.filter(pk=int(pos_id)).update(visible=False)

                users = User.objects.all()
                for item in users:
                    if (item.username == pos.user_name):

                        Profile.objects.filter(pk=request.user.profile.pk).update(expense=F('expense') - pos.money)
                        Profile.objects.filter(pk=item.profile.pk).update(expense=F('expense') + float(pos.money))

                        Profile.objects.filter(pk=request.user.profile.pk).update(transfer_expense_in_user=F('transfer_expense_in_user') + float(pos.money))
                        Profile.objects.filter(pk=item.profile.pk).update(having_transfer_expense_in_user=F('having_transfer_expense_in_user') + float(pos.money))

                        TransferObtainingMoney.objects.filter(pk=int(pos_id)).update(transfer_user=item)

                return redirect('/money/transfer/obtaining/in/expense/user')

        else:
            return redirect('/money/transfer/no/')


    context = {

        'transfers':transfers,

    }

    return render(request, 'transfer_vivod_in_activation_user_basket.html', context)
