
from django import forms
from apps.money.models import *


class PerfectAddForm(forms.ModelForm):
    class Meta:
        model = PerfectAdd
        fields = ('money',)
        exclude = ('user','key',)


class PayeerAddForm(forms.ModelForm):
    class Meta:
        model = PayeerAdd
        fields = ('money',)
        exclude = ('user', 'key', 'pay_signature',)


class PerfectSubtractForm(forms.ModelForm):
    class Meta:
        model = PerfectSubtract
        fields = ('money',)
        exclude = ('user',)

class PayeerSubtractForm(forms.ModelForm):
    class Meta:
        model = PayeerSubtract
        fields = ('money',)
        exclude = ('user',)


class TransferObtainingMoneyForm(forms.ModelForm):
    class Meta:
        model = TransferObtainingMoney
        fields = ('money','user_name',)
        exclude = ('user',)


class InternalTranslationForm(forms.ModelForm):
    class Meta:
        model = InternalTranslation
        fields = ('moneys',)
        exclude = ('user',)


class TransferPiggyBanksForm(forms.ModelForm):
    class Meta:
        model = TransferPiggyBanks
        fields = ('moneys',)
        exclude = ('user',)


class TransferExpenseForm(forms.ModelForm):
    class Meta:
        model = TransferExpenMoney
        fields = ('money',)
        exclude = ('user','payments','datetime',)


class TransferVivodInActivationForm(forms.ModelForm):
    class Meta:
        model = InternalTranslation
        fields = ('moneys',)
        exclude = ('user','datetime',)


class TransferBaliInActivationForm(forms.ModelForm):
    class Meta:
        model = TransferPiggyBanks
        fields = ('moneys',)
        exclude = ('user','datetime',)


class TransferVivodInActivationUserForm(forms.ModelForm):
    class Meta:
        model = TransferObtainingMoney
        fields = ('money','transfer_user','user_name',)
        exclude = ('user','datetime',)