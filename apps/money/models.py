# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


class PerfectAdd(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE )

    money = models.FloatField()

    key = models.CharField(max_length=200)

    visible = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '1. + Perfect Money'
        verbose_name_plural = '1. + Perfect Money'


class PayeerAdd(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    money = models.FloatField()

    key = models.CharField(max_length=200)

    pay_signature = models.CharField(max_length=200)

    visible = models.BooleanField(default=False)

    end = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    order_id = models.CharField(max_length=200)

    amount = models.FloatField(max_length=200)

    currency = models.CharField(max_length=200)

    description = models.CharField(max_length=200)

    class Meta:
        verbose_name = '1. + Payeer'
        verbose_name_plural = '1. + Payeer'


class PerfectSubtract(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    money = models.FloatField()

    purse = models.CharField(max_length=50)

    visible = models.BooleanField(default=False)

    end = models.BooleanField(default=False)

    refka = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '3. - Perfect Money'
        verbose_name_plural = '3. - Perfect Money'


class PayeerSubtract(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    money = models.FloatField()

    purse = models.CharField(max_length=50)

    visible = models.BooleanField(default=False)

    end = models.BooleanField(default=False)

    refka = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '2. - Payeer'
        verbose_name_plural = '2. - Payeer'


class TransferObtainingMoney(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user', on_delete=models.CASCADE)

    user_name = models.CharField(max_length=50, null=True, blank=True)

    transfer_user = models.ForeignKey(User, null=True, blank=True, related_name='transfer_user', on_delete=models.CASCADE)

    money = models.FloatField()

    visible = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '4. Перевод средст с вивода другому пользователю'
        verbose_name_plural = '4. Перевод средст с вивода другому пользователю'


class InternalTranslation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    moneys = models.FloatField()

    visible = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '3. Внутренний перевод'
        verbose_name_plural = '3. Внутренний перевод'


class TransferPiggyBanks(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    moneys = models.FloatField()

    visible = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '3. Внутренний перевод c копилки'
        verbose_name_plural = '3. Внутренний перевод c копилки'


class TransferExpenMoney(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    money = models.FloatField()

    payments = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = '5. Перевод на копилку с активации'
        verbose_name_plural = '5. Перевод на копилку с активации'