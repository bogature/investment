# Generated by Django 2.1.4 on 2019-03-30 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0007_auto_20190201_1501'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='payeeradd',
            options={'verbose_name': '1. + Payeer', 'verbose_name_plural': '1. + Payeer'},
        ),
        migrations.AlterModelOptions(
            name='payeersubtract',
            options={'verbose_name': '2. - Payeer', 'verbose_name_plural': '2. - Payeer'},
        ),
        migrations.AlterModelOptions(
            name='transferobtainingmoney',
            options={'verbose_name': '4. Перевод средст с вивода другому пользователю', 'verbose_name_plural': '4. Перевод средст с вивода другому пользователю'},
        ),
        migrations.AddField(
            model_name='perfectsubtract',
            name='end',
            field=models.BooleanField(default=False),
        ),
    ]
