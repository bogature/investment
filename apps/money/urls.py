
from django.conf.urls import url

from apps.money import views


urlpatterns = [

    url(r'^refill/$', views.refill, name='refill'),

    url(r'^issuance/$', views.issuance, name='issuance'),

    url(r'^refill-perfect/$', views.refill_perfect, name='refill_perfect'),

    url(r'^refill-payeer/$', views.refill_payeer, name='refill_payeer'),

    url(r'^refill-perfect/ok/$', views.refill_perfect_add, name='refill_perfect_ok'),

    url(r'^refill-perfect/(?P<perfect_pk>\w+)/$', views.refill_perfect_add, name='refill_perfect_add'),

    url(r'^refill-payeer/yes/$', views.refill_payeer_add, name='refill_payeer_add'),

    url(r'^issuance-perfect/$', views.issuance_perfect, name='issuance_perfect'),

    url(r'^issuance-payeer/$', views.issuance_payeer, name='issuance_payeer'),


    url(r'^perfect/admin/$', views.perfect_admin, name='perfect_admin'),



    url(r'^transfer/$', views.transfer_money, name='transfer_money'),

    url(r'^transferkopilka/$', views.transferkopilka, name='transferkopilka'),

    url(r'^transfervivod/$', views.transfervivod, name='transfervivod'),

    url(r'^transferuser/$', views.transferuser, name='transferuser'),

    url(r'^transfer/ok/$', views.transfer_money_ok, name='transfer_money_ok'),

    url(r'^transfer/ok/vivod/$', views.transfer_money_ok_vivod, name='transfer_money_ok_vivod'),

    url(r'^transfer/no/$', views.transfer_money_no, name='transfer_money_no'),


    url(r'^transfer/expense/bali/$', views.transfer_money_expense_in_bali, name='transfer_money_expense_in_bali'),

    url(r'^transfer/expense/bali/basket/$', views.transfer_money_expense_in_bali_basket, name='transfer_money_expense_in_bali_basket'),







    url(r'^transfer/obtaining/in/expense/$', views.transfer_money_vivod_in_activation, name='transfer_money_vivod_in_activation'),

    url(r'^transfer/obtaining/in/expense/basket/$', views.transfer_money_vivod_in_activation_basket, name='transfer_money_vivod_in_activation_basket'),


    url(r'^transfer/bali/in/expense/$', views.transfer_bali_in_activation, name='transfer_bali_in_activation'),

    url(r'^transfer/bali/in/expense/basket/$', views.transfer_bali_in_activation_basket, name='transfer_bali_in_activation_basket'),


    url(r'^transfer/obtaining/in/expense/user$', views.transfer_vivod_in_activation_user, name='transfer_vivod_in_activation_user'),

    url(r'^transfer/obtaining/in/expense/user/basket/$', views.transfer_vivod_in_activation_user_basket, name='transfer_bali_in_activation_basket'),

]