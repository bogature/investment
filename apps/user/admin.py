
from django.contrib import admin

from apps.user.models import Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'referal', 'status_referal_usd', 'new_status_usd', 'oborot_usd',)

admin.site.register(Profile, ProfileAdmin)
