# coding=utf-8
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.deposit.models import StatusBtc, StatusUsd


class Profile(models.Model):
    logo = models.ImageField('Logo', null=True, blank=True, upload_to='profile_images')

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    referal = models.ForeignKey(User, related_name='profile_referel', blank=True, null=True, on_delete=models.CASCADE)
    status_referal_usd = models.ForeignKey(StatusUsd, related_name='status_ref_usd', blank=True, null=True, on_delete=models.CASCADE)
    status_referal_btc = models.ForeignKey(StatusBtc, related_name='status_ref_btc', blank=True, null=True, on_delete=models.CASCADE)
    new_status_usd = models.ForeignKey(StatusUsd, related_name='status_usd', blank=True, null=True, on_delete=models.CASCADE)
    new_status_btc = models.ForeignKey(StatusBtc, related_name='status_btc', blank=True, null=True, on_delete=models.CASCADE)

    balance_usd = models.FloatField('Online Баласн USD', default=0)
    balance_btc = models.FloatField('Online Баласн BTC', default=0)

    zarab_usd = models.FloatField('Online Баланс вывода USD:', default=0)
    zarab_btc = models.FloatField('Online Баланс вывода BTC', default=0)

    zarab_usd_all = models.FloatField('Online Баланс вывода USD:', default=0)
    zarab_btc_all = models.FloatField('Online Баланс вывода BTC', default=0)

    bonus_usd = models.FloatField('Online Заработано Бонусов USD', default=0)
    bonus_btc = models.FloatField('Online Заработано Бонусов BTC', default=0)

    filled_usd = models.FloatField('Пополнил USD', default=0)
    filled_btc = models.FloatField('Пополнил BTC', default=0)

    out_usd = models.FloatField('Вывел USD', default=0)
    out_btc = models.FloatField('Вывел BTC', default=0)

    zarab_premia_usd = models.FloatField('Online Заработано Premia USD', default=0)
    zarab_premia_btc = models.FloatField('Online Заработано Premia BTC', default=0)

    out_bonus_usd = models.FloatField('Вывел Партнерские вознаграждения USD', default=0)
    out_bonus_btc = models.FloatField('Вывел Партнерские вознаграждения BTC', default=0)


    deposit_usd = models.FloatField('Cумма Депозит USD', default=0)
    deposit_btc = models.FloatField('Cумма Депозит BTC', default=0)


    sum_vivoda_usd = models.FloatField('Cумма вивода USD', default=0)
    sum_vivoda_btc = models.FloatField('Cумма вивода BTC', default=0)

    oborot_usd = models.FloatField('Оборот USD', default=0)
    oborot_btc = models.FloatField('Оборот BTC', default=0)

    expense = models.FloatField('Оборот BTC', default=0)

    phone = models.CharField('Телефон', blank=True, null=True, max_length=30)

    whatsapp = models.CharField(blank=True, null=True, max_length=30)

    viber = models.CharField(blank=True, null=True, max_length=30)

    skype = models.CharField(blank=True, null=True, max_length=30)

    url_facebook = models.CharField(blank=True, null=True, max_length=30)

    url_vk= models.CharField(blank=True, null=True, max_length=30)

    url_ok= models.CharField(blank=True, null=True, max_length=30)

    advcash= models.CharField(blank=True, null=True, max_length=30)

    bitcoin= models.CharField(blank=True, null=True, max_length=30)

    payeer= models.CharField(blank=True, null=True, max_length=30)

    perfectmoney= models.CharField(blank=True, null=True, max_length=30)

    visible = models.BooleanField(default=False)

    btn_active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.user

    class Meta:
        verbose_name = '1. Пользователи'
        verbose_name_plural = '1. Пользователи'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if (instance.profile.referal == None):
        user = User.objects.filter(pk=1)[0]
        instance.profile.referal = user
    instance.profile.save()