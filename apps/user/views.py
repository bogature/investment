import calendar
import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login

from apps.deposit.models import StatusUsd, StatusBtc
from apps.user.models import Profile
from .forms import SignupForm, UserForm, ProfileForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.mail import EmailMessage

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six



@login_required
def profile(request):

    data = datetime.datetime.today().weekday()
    status = StatusUsd.objects.all()
    day_calendar = calendar.day_name[datetime.datetime.today().weekday()]

    if request.method == "POST":
        start = request.POST.get('start')

        if(start == 'yes'):
            Profile.objects.filter(user=request.user).update(btn_active=True)

    context = {

        'data': data,
        'day_calendar': day_calendar,
        'status': status,

    }

    return render(request, 'profile.html', context)



@login_required
@transaction.atomic
def user_redirect(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile, files=request.FILES or None, data=request.POST or None)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, ('Your profile was successfully updated!'))
            return redirect('user:profile')
        else:
            messages.error(request, ('Please correct the error below.'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)

    return render(request, 'redirect_user.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })



class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )
account_activation_token = TokenGenerator()


def stsignup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your blog account.'
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            email.send()
            return render(request, 'confirm_email.html')
    else:
        form = SignupForm()
    return render(request, 'register.html', {'form': form})



def info(request, user_id):

    profile_user = get_object_or_404(User, pk=user_id)
    referrals = Profile.objects.filter(referal=profile_user).order_by('-id')

    context = {

        'referrals': referrals,
        'profile_user': profile_user,


    }

    return render(request, 'user_info.html', context)


def signup(request, user_pk):

    try:
        request.session['my_referal'] = user_pk
        my_referal = request.session['my_referal']
    except:
        request.session['my_referal'] = 1
        my_referal = 1

    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your blog account.'
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(mail_subject, message, to=[to_email]
            )
            email.send()
            return render(request, 'confirm_email.html')
    else:
        form = SignupForm()
    return render(request, 'register.html', {'form': form, 'my_referal': my_referal})


def activate(request, uidb64, token):

    try:
        my_referal = request.session['my_referal']
    except:
        request.session['my_referal'] = 1
        my_referal = 1

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True

        Profile.objects.filter(pk=user.pk).update(status_referal_usd=get_object_or_404(StatusUsd, pk=2),
                                                  status_referal_btc=get_object_or_404(StatusBtc, pk=2),
                                                  referal=get_object_or_404(Profile, pk=my_referal))

        user.save()
        login(request, user)
        return render(request, 'email_confirmation.html')
    else:
        return render(request, 'link_invalid.html')