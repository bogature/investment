
from django.conf.urls import url

from apps.user import views


urlpatterns = [

    url(r'^register/$', views.stsignup, name='stregister'),

    url(r'^register/(?P<user_pk>\d+)/$', views.signup , name='register'),

    url(r'^profile/$', views.profile, name='profile'),

    url(r'^redirect/',views.user_redirect, name='user_redirect'),

    url(r'^info/(?P<user_id>\d+)/$', views.info, name='info'),

    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            views.activate, name='activate'),
]