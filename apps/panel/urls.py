
from django.conf.urls import url

from apps.panel import views


urlpatterns = [

    url(r'^$', views.panel, name='panel'),

    url(r'deposit/$', views.panel_deposit, name='panel_deposit'),

    url(r'^panel_payeer_output/', views.panel_payeer_output, name='panel_payeer_output'),

    url(r'^history/usd/', views.panel_history_usd, name='panel_history_usd'),

]