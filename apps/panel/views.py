# coding=utf-8
import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.db.models import F
from django.shortcuts import render, redirect, get_object_or_404

from apps.deposit.models import DepositUsd, DepositBTC, CategoryUSD, HistoryUsd
from apps.money.PerfectMoney import PerfectMoney
from apps.money.models import PayeerSubtract
from apps.payeer.api import PayeerApi
from apps.user.models import Profile
from datetime import date


def panel(request):

    # # Баланс payeer
    account = 'P1012357967'
    api_id = '792548463'
    api_pass = 'j9d6BAvRGowgJyNN'
    balanse_payeer = PayeerApi(account, api_id, api_pass).get_balance()
    balanse_payeer = balanse_payeer.get('USD').get('DOSTUPNO')

    account = '3310706'
    password = 'zN3Ht7vV'

    pm = PerfectMoney(account, password)
    res = pm.balance()
    pr_balans_perfect = str(res.get('U19769559'))
    print(pr_balans_perfect)

    context = {

        'balanse_payeer': balanse_payeer,
        'pr_balans_perfect': pr_balans_perfect,

    }
    return render(request, 'panel.html', context)


def marketing_usd(request, deposit):

    if (deposit.user.profile.btn_active == True and deposit.activate == True):
        cat_depos = CategoryUSD.objects.all().order_by('-id')[0]
        deposit = DepositUsd.objects.filter(pk=deposit.pk)[0]

        datatime = datetime.datetime.now()
        if (datatime.day != deposit.datetime.day or datatime.month != deposit.datetime.month or datatime.year != deposit.datetime.year):
            DepositUsd.objects.filter(pk=deposit.pk).update(count_day=F('count_day')+1,
                                                            zarabotano=F('zarabotano')+(cat_depos.suma * deposit.sum)/100)

            Profile.objects.filter(user=deposit.user).update(zarab_usd=F('zarab_usd')+(cat_depos.suma * deposit.sum)/100,
                                                             zarab_usd_all=F('zarab_usd_all')+(cat_depos.suma * deposit.sum)/100)

            print("New history")
            new_history = HistoryUsd(users=deposit.user, deposit=deposit, category=cat_depos,
                                     sponsor=deposit.user, maseger='Deposit', number=(cat_depos.suma * deposit.sum)/100,
                                     datetime=date.today())
            new_history.save()

    else:
        cat_depos = CategoryUSD.objects.all().order_by('-id')[0]
        new_history = HistoryUsd(users=deposit.user, deposit=deposit, category=cat_depos,
                                 sponsor=deposit.user, maseger='No active button', number=0,
                                 datetime=date.today())
        new_history.save()

    if (deposit.count_day >= 365):
        DepositUsd.objects.filter(pk=deposit.pk).update(activate=False)


def panel_deposit(request):

    cat_usd = CategoryUSD.objects.all().order_by('-id')[0]

    count_all_deposit = DepositUsd.objects.filter(activate=True, end=False).count()
    count_active_deposit_usd = 0
    count_active_deposit_no_active_user_usd = 0
    count_history_usd = HistoryUsd.objects.filter(datetime=date.today(), bonus=False, oborot=False).count()
    active_deposit_usd = DepositUsd.objects.filter(activate=True, end=False)
    for depos in active_deposit_usd:
        if (depos.user.profile.btn_active == True):
            count_active_deposit_usd += 1
        else:
            count_active_deposit_no_active_user_usd += 1


    count_all_user = Profile.objects.all().count()
    count_active_user = Profile.objects.filter(btn_active=True).count()
    count_usd_depos = DepositUsd.objects.filter(user=request.user, end=False, activate=True).count()
    count_btc_depos = DepositBTC.objects.filter(user=request.user, end=False, activate=True).count()

    if request.method == "POST":
        name_category = request.POST.get('name_category')

        if(name_category == 'usd'):
            liva_usd = request.POST.get('liva')
            prava_usd = request.POST.get('prava')
            print(liva_usd)
            print(prava_usd)

            all_dep_usd = DepositUsd.objects.filter(end=False, activate=True)[int(liva_usd):int(prava_usd)]
            for dep in all_dep_usd:
                marketing_usd(request, dep)

            return redirect(request.path)

        if (name_category == 'bts'):
            all_dep_usd = DepositBTC.objects.filter(end=False, activate=True)
            for dep in all_dep_usd:
                print(dep)
                print(dep.user)


            return redirect(request.path)

        if (name_category == 'active_user'):
            all_profile = Profile.objects.filter(btn_active=True)
            for profile in all_profile:
                print(profile)
                Profile.objects.filter(pk=profile.pk).update(btn_active=False)

            return redirect(request.path)

    context = {

        'count_all_user': count_all_user,
        'count_active_user': count_active_user,
        'count_usd_depos': count_usd_depos,
        'count_btc_depos': count_btc_depos,

        'count_active_deposit_usd': count_active_deposit_usd,
        'count_active_deposit_no_active_user_usd': count_active_deposit_no_active_user_usd,
        'count_all_deposit': count_all_deposit,
        'count_history_usd': count_history_usd,
        'cat_usd': cat_usd,

    }
    return render(request, 'panel_deposit.html', context)



def panel_payeer_output(request):

    account = 'P1012357967'
    api_id = '792548463'
    api_pass = 'j9d6BAvRGowgJyNN'
    balanse_payeer = PayeerApi(account, api_id, api_pass).get_balance()
    balanse_payeer = balanse_payeer.get('USD').get('DOSTUPNO')

    payeer_outputs = PayeerSubtract.objects.filter(visible=True)

    if request.method == "POST":
        pos_id = request.POST.get('position')
        perfect_sub = PayeerSubtract.objects.filter(pk=(int(pos_id)))[0]

        if (perfect_sub.visible == True and perfect_sub.end == False):
            PayeerSubtract.objects.filter(pk=perfect_sub.pk).update(visible=False, end=True)

            account = 'P1012357967'
            api_id = '792548463'
            api_pass = 'j9d6BAvRGowgJyNN'

            endmoney = (perfect_sub.money - (float(perfect_sub.money) * float(3.5) / 100))
            #
            # Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(obtaining_money=F("obtaining_money") - perfect_sub.money)
            # Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(pay_del=F("pay_del") + perfect_sub.money)

            # Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(zarab_usd=F("zarab_usd") - perfect_sub.money)
            # Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(pay_del=F("pay_del") + perfect_sub.money)

            if perfect_sub.refka == False:

                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    zarab_usd=F("zarab_usd") - perfect_sub.money)
                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    out_usd=F("out_usd") + perfect_sub.money)

            else:

                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    bonus_usd=F("bonus_usd") - perfect_sub.money)
                Profile.objects.filter(pk=perfect_sub.user.profile.pk).update(
                    out_bonus_usd=F("out_bonus_usd") + perfect_sub.money)

            PayeerApi(account, api_id, api_pass).transfer(sum=round(endmoney, 2), to=str(perfect_sub.purse), cur_in='USD', cur_out='USD', comment='Deltus.biz')
            return redirect(request.path)


    context = {

        'payeer_outputs': payeer_outputs,
        'balanse_payeer': balanse_payeer,

    }

    return render(request, 'panel_payeer_output.html', context)


def panel_history_usd(request):

    historys = HistoryUsd.objects.filter(datetime=date.today()).order_by('-id')

    context = {

        'historys': historys,

    }
    return render(request, 'panel_history_usd.html', context)

