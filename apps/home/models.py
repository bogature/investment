from django.db import models


class Contacts(models.Model):
    email = models.EmailField()

    tema = models.CharField(max_length=20)

    text = models.TextField()

    class Meta:
        verbose_name = ' Повідомлення'
        verbose_name_plural = ' Повідомлення'


class Faq(models.Model):
    name = models.CharField(max_length=100)
    info = models.CharField(max_length=10000)

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = ' FAQ'
