from django.shortcuts import render, redirect

from apps.deposit.models import StatusUsd, BonusContribution, StatusBtc
from apps.home.models import Contacts, Faq
from apps.home.forms import *


def home(request):
    context = {

    }
    return render(request, 'home.html', context)


def contacts(request):
    form = ContactsForm
    if request.method == "POST":
        form = ContactsForm(request.POST)
        if form.is_valid():
            inst = form.save(commit=False)
            if (request.user.is_active):
                inst.profile = request.user
            inst.save()
            return redirect('/help/')
    context = {
        'form': form,
    }
    return render(request, 'contacts.html', context)


def yes_help(request):
    context = {

    }
    return render(request, 'yes_help.html', context)


def about(request):
    context = {
    }
    return render(request, 'about.html', context)


def plans(request):
    context = {
    }
    return render(request, 'plans.html', context)


def faq(request):
    faqs = Faq.objects.all()
    context = {
        'faqs': faqs,
    }
    return render(request, 'faq.html', context)


def partner(request):

    status_usd = StatusUsd.objects.all()
    status_btc = StatusBtc.objects.all()
    bonus = BonusContribution.objects.all()[0:5]
    bonus_two_level = BonusContribution.objects.all()[5:]

    context = {

        'status_usd': status_usd,
        'status_btc': status_btc,
        'bonus': bonus,
        'bonus_two_level': bonus_two_level,

    }
    return render(request, 'partner.html', context)