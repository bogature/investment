
from django.contrib import admin
from apps.home.models import *


class ContactsAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'tema', 'text',)


admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Faq)