from django.conf.urls import url
from apps.home import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^contacts/', views.contacts, name='contacts'),
    url(r'^help/', views.yes_help, name='yes_help'),
    url(r'^about/', views.about, name='about'),
    url(r'^plans/', views.plans, name='plans'),
    url(r'^partner/', views.partner, name='partner'),
    url(r'^faq/', views.faq, name='faq'),
]