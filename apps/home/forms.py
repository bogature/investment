from django import forms

from apps.home.models import Contacts


class ContactsForm(forms.ModelForm):
    class Meta:
        model = Contacts
        fields = ( 'email', 'text', 'tema',)