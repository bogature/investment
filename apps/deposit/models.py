# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
from pytz import unicode


class CategoryUSD(models.Model):
    suma = models.FloatField(default=0)

    datetime = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return unicode(self.suma)

    class Meta:
        verbose_name = '1.Категории USD'
        verbose_name_plural = '1. Категории USD'


class CategoryBTC(models.Model):
    suma = models.FloatField(default=0)

    datetime = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return unicode(self.suma)

    class Meta:
        verbose_name = '2.Категории BTC'
        verbose_name_plural = '2. Категории BTC'


class DepositUsd(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_usd',)

    sum = models.FloatField(default=0)

    count_day = models.IntegerField(default=0)

    datetime = models.DateTimeField(null=True, blank=True)

    zarabotano = models.FloatField(default=0)

    activate = models.BooleanField(default=False)

    end = models.BooleanField(default=False)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '3.Usd'
        verbose_name_plural = '3. Usd'


class DepositBTC(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_btc',)

    sum = models.FloatField(default=0)

    count_day = models.IntegerField(default=0)

    datetime = models.DateTimeField(null=True, blank=True)

    zarabotano = models.FloatField(default=0)

    activate = models.BooleanField(default=False)

    end = models.BooleanField(default=False)


    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '4.BTC'
        verbose_name_plural = '4. BTC'



class HistoryUsd(models.Model):
    users = models.ForeignKey(User, on_delete=models.CASCADE, related_name='history_users_usd',)

    deposit = models.ForeignKey(DepositUsd, on_delete=models.CASCADE, related_name='deposit_usd', null=True, blank=True)

    category = models.ForeignKey(CategoryUSD, on_delete=models.CASCADE, related_name='deposit_usd', null=True, blank=True)

    sponsor = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='history_sponsor_usd',)

    maseger = models.CharField('Сообщение', max_length=50, null=True, blank=True)

    number = models.FloatField()

    plus = models.BooleanField(default=True)

    bonus = models.BooleanField(default=False)

    oborot = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '5. История Usd'
        verbose_name_plural = '5. История Usd'


class HistoryBTC(models.Model):
    users = models.ForeignKey(User, on_delete=models.CASCADE, related_name='history_users_btc',)

    deposit = models.ForeignKey(DepositBTC, on_delete=models.CASCADE, related_name='deposit_btc', null=True, blank=True)

    category = models.ForeignKey(CategoryBTC, on_delete=models.CASCADE, related_name='deposit_usd', null=True, blank=True)

    sponsor = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='history_sponsor_btc',)

    maseger = models.CharField('Сообщение', max_length=50, null=True, blank=True)

    number = models.FloatField()

    plus = models.BooleanField(default=True)

    bonus = models.BooleanField(default=False)

    oborot = models.BooleanField(default=False)

    datetime = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '6. История BTC'
        verbose_name_plural = '6. История BTC'




class BonusContribution(models.Model):

    name = models.CharField('Назва', max_length=50, null=True, blank=True)

    level = models.IntegerField(null=True, blank=True)

    number = models.FloatField()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = '7. Партнерка с вклада'
        verbose_name_plural = '7. Партнерка с вклада'


class BonusIncome(models.Model):

    name = models.CharField('Назва', max_length=50, null=True, blank=True)

    level = models.IntegerField(null=True, blank=True)

    number = models.FloatField()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = '8. Рефка с дохода'
        verbose_name_plural = '8. Рефка с дохода'


class StatusUsd(models.Model):

    name = models.CharField('Назва', max_length=50, null=True, blank=True)

    level = models.IntegerField(null=True, blank=True)

    oborot = models.FloatField(null=True, blank=True, default=0)

    premia = models.FloatField(null=True, blank=True, default=0)

    max_level_ref = models.CharField(max_length=200, null=True, blank=True)

    desc_max_level_ref = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '9. Статус Usd'
        verbose_name_plural = '9. Статус Usd'


class StatusBtc(models.Model):

    name = models.CharField('Назва', max_length=50, null=True, blank=True)

    level = models.IntegerField(null=True, blank=True)

    oborot = models.FloatField(null=True, blank=True, default=0)

    premia = models.FloatField(null=True, blank=True, default=0)

    max_level_ref = models.CharField(max_length=200, null=True, blank=True)

    desc_max_level_ref = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '9. Статус Btc'
        verbose_name_plural = '9. Статус Btc'

