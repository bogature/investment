# from django.contrib import admin
#
from django.contrib import admin

from apps.deposit.models import HistoryBTC, HistoryUsd, DepositUsd, DepositBTC, BonusContribution, BonusIncome, \
    CategoryBTC, CategoryUSD, StatusUsd, StatusBtc


class DepositUsdAdmin(admin.ModelAdmin):
    list_display = ('id', 'count_day', 'datetime', 'zarabotano', 'activate',)

class DepositBTCAdmin(admin.ModelAdmin):
    list_display = ('id', 'count_day', 'datetime', 'zarabotano', 'activate',)

class StatusUsdAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'level', 'oborot', 'premia', 'max_level_ref',)

class StatusBtcAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'level', 'oborot', 'premia', 'max_level_ref',)

class BonusContributionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'number',)


class HistoryUsdAdmin(admin.ModelAdmin):
    list_display = ('id', 'users', 'sponsor', 'maseger','plus','bonus','oborot','datetime',)

class HistoryBTCAdmin(admin.ModelAdmin):
    list_display = ('id', 'users', 'sponsor', 'maseger','plus','bonus','oborot','datetime',)


admin.site.register(DepositUsd, DepositUsdAdmin)
admin.site.register(DepositBTC, DepositBTCAdmin)
admin.site.register(HistoryUsd, HistoryUsdAdmin)
admin.site.register(HistoryBTC, HistoryBTCAdmin)
admin.site.register(BonusContribution, BonusContributionAdmin)
# admin.site.register(BonusIncome)
admin.site.register(CategoryUSD)
admin.site.register(CategoryBTC)
admin.site.register(StatusUsd, StatusUsdAdmin)
admin.site.register(StatusBtc, StatusBtcAdmin)