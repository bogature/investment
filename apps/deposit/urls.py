
from django.conf.urls import url
from apps.deposit import views


urlpatterns = [

    url(r'^$', views.deposit, name='deposit'),

    url(r'history/$', views.history_deposit, name='history_deposit'),

    url(r'status/$', views.status, name='status'),

    url(r'^deposit/history/usd/(?P<deposit_id>\d+)/$', views.this_deposit_history_usd, name='this_deposit_history_usd'),

    url(r'^deposit/history/btc/(?P<deposit_id>\d+)/$', views.this_deposit_history_btc, name='this_deposit_history_btc'),

]