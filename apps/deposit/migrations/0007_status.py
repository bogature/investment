
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deposit', '0006_auto_20190426_0820'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Назва')),
                ('level', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': '9. Статус',
                'verbose_name_plural': '9. Статус',
            },
        ),
    ]
