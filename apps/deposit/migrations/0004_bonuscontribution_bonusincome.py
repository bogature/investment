# Generated by Django 2.2 on 2019-04-22 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deposit', '0003_auto_20190422_1440'),
    ]

    operations = [
        migrations.CreateModel(
            name='BonusContribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Назва')),
                ('level', models.IntegerField(blank=True, null=True)),
                ('number', models.FloatField()),
                ('pribel', models.FloatField()),
            ],
            options={
                'verbose_name': '5. Партнерка с вклада',
                'verbose_name_plural': '5. Партнерка с вклада',
            },
        ),
        migrations.CreateModel(
            name='BonusIncome',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Назва')),
                ('level', models.IntegerField(blank=True, null=True)),
                ('number', models.FloatField()),
                ('pribel', models.FloatField()),
            ],
            options={
                'verbose_name': '6. Рефка с дохода',
                'verbose_name_plural': '6. Рефка с дохода',
            },
        ),
    ]
