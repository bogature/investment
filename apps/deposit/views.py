import datetime

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import F
from django.shortcuts import render, redirect, get_object_or_404
from django_registration.backends.one_step.views import User

from apps.deposit.models import DepositUsd, DepositBTC, HistoryUsd, HistoryBTC, BonusContribution, BonusIncome, \
    StatusUsd, StatusBtc
from apps.user.models import Profile


#
#
# def next_user_ref(user):
#     user = get_object_or_404(User, pk=user)
#     return user.profile.referal.pk
#
#
# def get_list_refferals(request):
#     list_user = []
#     i=0
#     flag = True
#     list_user.append(request.user.profile.referal.pk)
#
#     while i <= 10 and flag:
#         a = list_user[i]
#         next = next_user_ref(a)
#
#         list_user.append(next)
#
#         if (a == next):
#             flag = False
#             list_user.remove(next)
#
#
#         i += 1
#
#     i=0
#     for item_user in list_user:
#         i+=1
#         print("Index = " + str(i))
#         print("User:" + str(item_user))
#         new_hist = HistoryUsd(users=get_object_or_404(User, pk=item_user), sponsor=request.user, maseger="Bonus",
#                               number=float(1), bonus=True, datetime=datetime.datetime.now())
#         new_hist.save()
#
#     return list_user
#
#
# def get_nenx_user(user):
#     user = get_object_or_404(User, pk=user)
#     if user.profile.referal != user:
#         return user.profile.referal.pk
#     else:
#         return 0
#
#
# def start_bonuss(request, suma_usd):
#     print("start bonus")
#
    # end_list = []
    # # end_list.append(get_nenx_user(request.user.pk))
    # end_list.append(request.user.pk)
    #
    # i = 0
    # flag = True
    # while i < 22 and flag:
    #     a = end_list[i]
    #     print("a = ")
    #     print(a)
    #     if (a != 0):
    #         end_list.append(get_nenx_user(a))
    #     else:
    #         flag = False
    #     i += 1
#
#     print(end_list)
#
#     for item in end_list:
#         if item != 0:
#             user = get_object_or_404(User, pk=item)
#             if user.profile.new_status_usd is not None:
#                 if int(user.profile.new_status_usd.level) >= int(item):
#                     print("Add : " + str(user.profile.new_status_usd) + " user = " + str(user))
#                     Profile.objects.filter(user=user).update(deposit_usd=F('deposit_usd') + float(suma_usd))
#                     new = HistoryUsd(users=user, sponsor=request.user, maseger="Add oborot", number=float(suma_usd), oborot=True, datetime=datetime.datetime.now())
#                     new.save()
#

def get_nenx_user(user):
    user = get_object_or_404(User, pk=user)
    if user.profile.referal != user:
        return user.profile.referal.pk
    else:
        return 0


def refca_users(user, suma_usd):
    user = get_object_or_404(User, pk=user.pk)
    print("Referalka")

    end_list = []
    end_list.append(user.profile.referal.pk)

    i = 0
    flag = True
    while i < 24 and flag:
        a = end_list[i]
        if (a != 0):
            end_list.append(get_nenx_user(a))
        else:
            flag = False
        i += 1

    print(end_list)

    i = 1
    while i < len(end_list) - 1 and i <= 10:
        bonus = get_object_or_404(BonusContribution, pk=i)

        ref_user = Profile.objects.filter(user=get_object_or_404(User, pk=end_list[i-1]))[0]
        if (ref_user.status_referal_usd != None):
            if (ref_user.status_referal_usd.pk >= i):
                print("Vhod")

                if i <= 1:
                    print("Refka1: " + str(ref_user.user) + " " + str((float(suma_usd) * bonus.number) / 100))
                    Profile.objects.filter(user=ref_user.user).update(bonus_usd=F('bonus_usd') + (float(suma_usd) * bonus.number)/100 )

                    new = HistoryUsd(users=ref_user.user, sponsor=user, maseger="Refka (" + str(user) + ")", number=float((float(suma_usd) * bonus.number)/100),
                                     bonus=True, datetime=datetime.datetime.now())
                    new.save()

                else:
                    if (ref_user.status_referal_usd.pk >= i+1):
                        print("Refka2: " + str(ref_user.user) + " " + str((float(suma_usd) * bonus.number) / 100))
                        Profile.objects.filter(user=ref_user.user).update(
                            bonus_usd=F('bonus_usd') + (float(suma_usd) * bonus.number) / 100)

                        new = HistoryUsd(users=ref_user.user, sponsor=user, maseger="Refka (" + str(user) + ")",
                                         number=float((float(suma_usd) * bonus.number) / 100),
                                         bonus=True, datetime=datetime.datetime.now())
                        new.save()

        i+=1

    print("Start oborot !!!")

    i=0
    while i < len(end_list) - 1:

        ref_user = Profile.objects.filter(user=get_object_or_404(User, pk=end_list[i]))[0]

        if i <= 1:
            print("Oborot 1: " + str(ref_user.user) + " " + str(suma_usd))
            Profile.objects.filter(user=ref_user.user).update(oborot_usd=F('oborot_usd') + (float(suma_usd)))
            check_status_usd(ref_user.user)
        else:
            if (ref_user.new_status_usd != None):
                if (ref_user.new_status_usd.pk >= i+1):
                    print("Oborot 2: " + str(ref_user.user) + " - " + str(suma_usd))
                    Profile.objects.filter(user=ref_user.user).update(oborot_usd=F('oborot_usd') + (float(suma_usd)))
                    check_status_usd(ref_user.user)

        i += 1


def deposit(request):
    massag = ""
    massag_btc = ""

    bonus_contribution = BonusContribution.objects.all()
    bonus_income = BonusIncome.objects.all()

    if request.method == "POST":
        name_category = request.POST.get('name_category')

        if(name_category == 'usd'):
            suma_usd = request.POST.get('suma_usd')

            if (float(suma_usd) >= 30 and float(suma_usd) <= 30000):
                if (request.user.profile.balance_usd >= float(suma_usd)):
                    Profile.objects.filter(user=request.user).update(balance_usd=F("balance_usd") - float(suma_usd))
                    Profile.objects.filter(user=request.user).update(deposit_usd=F("deposit_usd") + float(suma_usd))

                    new_dep = DepositUsd(user=request.user, sum=suma_usd, datetime=datetime.datetime.now(), activate=True)
                    new_dep.save()

                    refca_users(request.user, int(suma_usd))
                    return redirect(request.path)
                else:
                    massag = "У вас не хватает средств. У вас на балансе: " + str(request.user.profile.balance_usd)
            else:
                massag = "Минимальная сумма $10"


        if (name_category == 'bts'):
            suma_bts = request.POST.get('suma_bts')
            if (request.user.profile.balance_btc >= float(suma_bts)):
                # print(suma_bts)
                new_dep = DepositBTC(user=request.user, sum=suma_bts, datetime=datetime.datetime.now(), activate=True)
                new_dep.save()
                return redirect(request.path)
            else:
                massag_btc = "Минимальная сумма "

    dep_usd = DepositUsd.objects.filter(user=request.user, activate=True).order_by('-id')
    dep_btc = DepositBTC.objects.filter(user=request.user, activate=True).order_by('-id')

    page1 = request.GET.get('page', 1)
    paginator1 = Paginator(dep_usd, 10)
    try:
        items_usd = paginator1.page(page1)
    except PageNotAnInteger:
        items_usd = paginator1.page(1)
    except EmptyPage:
        items_usd = paginator1.page(paginator1.num_pages)

    page2 = request.GET.get('page', 1)
    paginator2 = Paginator(dep_btc, 10)
    try:
        items_btc = paginator2.page(page2)
    except PageNotAnInteger:
        items_btc = paginator2.page(1)
    except EmptyPage:
        items_btc = paginator2.page(paginator2.num_pages)

    context = {

        'dep_usd': items_usd,
        'dep_btc': items_btc,

        'bonus_contributions': bonus_contribution,
        'bonus_incomes': bonus_income,
        'massag': massag,
        'massag_btc': massag_btc,

    }
    return render(request, 'deposit.html', context)


def history_deposit(request):

    his_dep_usd = HistoryUsd.objects.filter(users=request.user).order_by('-id')
    his_dep_btc = HistoryBTC.objects.filter(users=request.user).order_by('-id')

    page = request.GET.get('page', 1)
    paginator = Paginator(his_dep_usd, 10)
    try:
        items_usd = paginator.page(page)
    except PageNotAnInteger:
        items_usd = paginator.page(1)
    except EmptyPage:
        items_usd = paginator.page(paginator.num_pages)

    page = request.GET.get('page', 1)
    paginator = Paginator(his_dep_btc, 10)
    try:
        items_btc = paginator.page(page)
    except PageNotAnInteger:
        items_btc = paginator.page(1)
    except EmptyPage:
        items_btc = paginator.page(paginator.num_pages)

    context = {

        'his_dep_usd': items_usd,
        'his_dep_btc': items_btc,

    }
    return render(request, 'history_deposit.html', context)


def this_deposit_history_usd(request, deposit_id):

    his_dep_usd = HistoryUsd.objects.filter(users=request.user, deposit=deposit_id).order_by('-id')

    page = request.GET.get('page', 1)
    paginator = Paginator(his_dep_usd, 30)
    try:
        items_usd = paginator.page(page)
    except PageNotAnInteger:
        items_usd = paginator.page(1)
    except EmptyPage:
        items_usd = paginator.page(paginator.num_pages)

    context = {

        'his_dep': items_usd,

    }
    return render(request, 'this_deposit_history.html', context)


def this_deposit_history_btc(request, deposit_id):

    his_dep_btc = HistoryBTC.objects.filter(users=request.user, deposit=deposit_id).order_by('-id')

    page = request.GET.get('page', 1)
    paginator = Paginator(his_dep_btc, 30)
    try:
        items_btc = paginator.page(page)
    except PageNotAnInteger:
        items_btc = paginator.page(1)
    except EmptyPage:
        items_btc = paginator.page(paginator.num_pages)

    context = {

        'his_dep': items_btc,

    }
    return render(request, 'this_deposit_history.html', context)


def check_status_usd(user):
    user = get_object_or_404(User, pk=user.pk)

    count_all_status = StatusUsd.objects.all().count()

    if (user.profile.new_status_usd != None):

        if (user.profile.new_status_usd.pk != int(count_all_status)):
            next_status = get_object_or_404(StatusUsd, pk=(user.profile.new_status_usd.pk) + 1)
            print(user.profile.oborot_usd)
            print(next_status.oborot)

            if (user.profile.oborot_usd > next_status.oborot):
                print("Perehod next user !!! " + str(next_status))
                if (next_status.max_level_ref != None):
                    Profile.objects.filter(user=user).update(status_referal_usd=get_object_or_404(StatusUsd, pk=next_status.max_level_ref))

                Profile.objects.filter(user=user).update(new_status_usd=get_object_or_404(StatusUsd, pk=next_status.pk))

                Profile.objects.filter(user=user).update(zarab_usd=F('zarab_usd') + float(next_status.premia))
                Profile.objects.filter(user=user).update(zarab_premia_usd=F('zarab_premia_usd') + float(next_status.premia))

                new = HistoryUsd(users=user, sponsor=user, maseger="Premium", number=float(next_status.premia), oborot=True,
                                 datetime=datetime.datetime.now())
                new.save()

                count_all_status = StatusUsd.objects.all().count()
                if (next_status.pk != int(count_all_status)):
                    check_status_usd(user)
    else:
        next_status = get_object_or_404(StatusUsd, pk=1)
        print(user.profile.oborot_usd)
        print(next_status.oborot)

        if (user.profile.oborot_usd > next_status.oborot):
            print("Perehod next user !!! " + str(next_status))
            Profile.objects.filter(user=user).update(new_status_usd=get_object_or_404(StatusUsd, pk=next_status.pk))

            Profile.objects.filter(user=user).update(zarab_usd=F('zarab_usd') + float(next_status.premia))
            Profile.objects.filter(user=user).update(zarab_premia_usd=F('zarab_premia_usd') + float(next_status.premia))

            new = HistoryUsd(users=user, sponsor=user, maseger="Premium", number=float(next_status.premia), oborot=True,
                             datetime=datetime.datetime.now())
            new.save()

            count_all_status = StatusUsd.objects.all().count()
            if (next_status.pk != int(count_all_status)):
                check_status_usd(user)


def status(request):

    all_status_usd = StatusUsd.objects.all()
    all_status_btc = StatusBtc.objects.all()


    check_status_usd(request.user)

    context = {

        'all_status_usd': all_status_usd,
        'all_status_btc': all_status_btc,

    }
    return render(request, 'status.html', context)
