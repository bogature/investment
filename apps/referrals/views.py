from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render

from apps.user.models import Profile


def referrals(request):

    referrals = Profile.objects.filter(referal=request.user).order_by('-id')

    context = {

        'referrals': referrals,

    }
    return render(request, 'referrals.html', context)