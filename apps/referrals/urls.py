
from django.conf.urls import url
from apps.referrals import views


urlpatterns = [

    url(r'^$', views.referrals, name='referrals'),

]