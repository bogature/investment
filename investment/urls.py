
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns

urlpatterns = i18n_patterns(
# urlpatterns = (
    url(r'^i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
    url(r'^', include(('apps.home.urls', 'home'), namespace='home')),
    url(r'^user/', include(('apps.user.urls', 'user'), namespace='user')),
    url(r'^money/', include(('apps.money.urls', 'money'), namespace='money')),
    url(r'^referrals/', include(('apps.referrals.urls', 'referrals'), namespace='referrals')),
    url(r'^deposit/', include(('apps.deposit.urls', 'deposit'), namespace='deposit')),
    url(r'^panel/', include(('apps.panel.urls', 'panel'), namespace='panel')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
)
